/* eslint unicorn/filename-case: "off", func-names: "off", camelcase: "off", no-unused-vars: "off" */

const convert = require('color-convert');

module.exports = function (iface) {
    const {mqttPub, mqttSub, mqttStatus, log, Service, Characteristic} = iface;

    return function createService_Lightbulb(acc, settings, subtype) {
        const current = {
            on: false,
            hue: 0,
            sat: 0,
            bri: 0
        };

        function parsRgbStringToHsv(str) {
          arr = String(str).split(';');
          const r = parseInt(arr[0]);
          const g = parseInt(arr[1]);
          const b = parseInt(arr[2]);
          const [hue, sat, bri] = convert.rgb.hsv([r, g, b]);

          return {
            hue: hue,
            sat: sat,
            bri: bri
          };
        }

        function publishRGB() {
            if (settings.topic.setRGB) {
                if (current.on) {
                    const rgb = convert.hex.rgb(
                      convert.rgb.hex(
                        convert.hsv.rgb([current.hue, current.sat, current.bri])
                      )
                    );
                    val = ''+ rgb[0] + ';' + rgb[1] + ';' + rgb[2];
                    mqttPub(settings.topic.setRGB, val);
                } else {
                    mqttPub(settings.topic.setRGB, '0;0;0');
                }
            }
        }

        if (settings.topic.statusRGB) {
            mqttSub(settings.topic.statusRGB, settings.json.statusRGB, val => {
                hsv = parsRgbStringToHsv(val);
                current.hue = hsv.hue;
                current.sat = hsv.sat;
                current.bri = hsv.bri;
                current.on = hsv.bri > 0;
                acc.getService(subtype)
                    .updateCharacteristic(Characteristic.On, hsv.bri > 0)
                    .updateCharacteristic(Characteristic.Hue, hsv.hue)
                    .updateCharacteristic(Characteristic.Saturation, hsv.sat)
                    .updateCharacteristic(Characteristic.Brightness, hsv.bri);
            });
        }

        acc.addService(Service.Lightbulb, settings.name, subtype)
            .getCharacteristic(Characteristic.On)
            .on('set', (value, callback) => {
                log.debug('< hap set', settings.name, 'On', value);
                current.on = value;
                publishRGB();
                const payload = value ? settings.payload.onTrue : settings.payload.onFalse;
                if (!settings.topic.setBrightness && mqttStatus[settings.topic.statusOn] !== payload) {
                    // TODO test!
                    if ((settings.topic.setOn !== settings.topic.setBrightness) || !value) {
                        mqttPub(settings.topic.setOn, payload);
                    } else {
                        // This should prevent flickering while dimming lights that use
                        // the same topic for On and Brightness, e.g. Homematic Dimmers
                        setTimeout(() => {
                            if (!mqttStatus[settings.topic.statusBrightness]) {
                                mqttPub(settings.topic.setOn, payload);
                            }
                        }, 300);
                    }
                }
                callback();
            });

        mqttSub(settings.topic.statusOn, settings.json.statusOn, val => {
            const on = val !== settings.payload.onFalse;
            log.debug('> hap update', settings.name, 'On', on);
            current.on = on;
            acc.getService(subtype)
                .updateCharacteristic(Characteristic.On, on);
        });

        acc.getService(subtype)
            .getCharacteristic(Characteristic.On)
            .on('get', callback => {
                log.debug('< hap get', settings.name, 'On');
                val = mqttStatus(settings.topic.statusOn, settings.json.statusOn);
                const on = val !== settings.payload.onFalse;
                log.debug('> hap re_get', settings.name, 'On', on);
                callback(null, on);
            });

        /* istanbul ignore else */
        if (settings.topic.setBrightness) {
            acc.getService(subtype)
                .addCharacteristic(Characteristic.Brightness)
                .on('set', (value, callback) => {
                    log.debug('< hap set', settings.name, 'Brightness', value);
                    current.bri = value;
                    publishRGB();
                    /* istanbul ignore next */
                    const bri = (value * (settings.payload.brightnessFactor || 1)) || 0;
                    //mqttPub(settings.topic.setBrightness, bri);
                    callback();
                });

            /* istanbul ignore else */
            if (settings.topic.statusBrightness) {
                mqttSub(settings.topic.statusBrightness, settings.json.statusBrightness, val => {
                    hsv = parsRgbStringToHsv(val);

                    acc.getService(subtype)
                        .updateCharacteristic(Characteristic.Brightness, hsv.bri);
                });

                acc.getService(subtype)
                    .getCharacteristic(Characteristic.Brightness)
                    .on('get', callback => {
                        log.debug('< hap get', settings.name, 'Brightness');
                        val = mqttStatus(settings.topic.statusBrightness, settings.json.statusBrightness);
                        hsv = parsRgbStringToHsv(val);

                        log.debug('> hap re_get', settings.name, 'Brightness', hsv.bri);
                        callback(null, hsv.bri);
                    });
            }
        }

        /* istanbul ignore else */
       if (settings.topic.setSaturation) {
           acc.getService(subtype)
               .addCharacteristic(Characteristic.Saturation)
               .on('set', (value, callback) => {
                   log.debug('< hap set', settings.name, 'Saturation', value);
                   current.sat = value;
                   publishRGB();
                   /* istanbul ignore next */
                   const sat = (value * (settings.payload.saturationFactor || 1)) || 0;
                   //mqttPub(settings.topic.setSaturation, sat);
                   callback();
               });
           /* istanbul ignore else */
           if (settings.topic.statusSaturation) {
               mqttSub(settings.topic.statusSaturation, settings.json.statusSaturation, val => {
                   hsv = parsRgbStringToHsv(val);
                   const sat = (hsv.sat / (settings.payload.saturationFactor || 1)) || 0;
                   log.debug('> hap update', settings.name, 'Saturation', sat);
                   current.sat = sat;
                   acc.getService(subtype)
                       .updateCharacteristic(Characteristic.Saturation, sat);
               });
               acc.getService(subtype)
                     .getCharacteristic(Characteristic.Saturation)
                   .on('get', callback => {
                       log.debug('< hap get', settings.name, 'Saturation');
                       /* istanbul ignore next */
                       val = mqttStatus(settings.topic.statusSaturation, settings.json.statusSaturation);
                       hsv = parsRgbStringToHsv(val);

                       log.debug('> hap re_get', settings.name, 'Saturation', hsv.sat);
                       callback(null, hsv.sat);
                   });
           }
       }


        /* istanbul ignore else */
        if (settings.topic.setHue) {
            acc.getService(subtype)
                .addCharacteristic(Characteristic.Hue)
                .on('set', (value, callback) => {
                    log.debug('< hap set', settings.name, 'Hue', value);
                    current.hue = value;
                    publishRGB();
                    /* istanbul ignore next */
                    const hue = (value * (settings.payload.hueFactor || 1));
                    mqttPub(settings.topic.setHue, hue);
                    callback();
                });
            /* istanbul ignore else */
            if (settings.topic.statusHue) {
                mqttSub(settings.topic.statusHue, settings.json.statusHue, val => {
                    /* istanbul ignore next */
                    hsv = parsRgbStringToHsv(val);
                    log.debug('> hap update', settings.name, 'Hue', hsv.hue);
                    current.hue = hsv.hue;
                    acc.getService(subtype)
                        .updateCharacteristic(Characteristic.Hue, hsv.hue);
                });
                acc.getService(subtype)
                    .getCharacteristic(Characteristic.Hue)
                    .on('get', callback => {
                        log.debug('< hap get', settings.name, 'Hue');
                        val = mqttStatus(settings.topic.statusHue, settings.json.statusHue);
                        hsv = parsRgbStringToHsv(val);

                        log.debug('> hap re_get', settings.name, 'Hue', hsv.hue);

                        callback(null, hsv.hue);
                    });
            }
        }


    };
};
